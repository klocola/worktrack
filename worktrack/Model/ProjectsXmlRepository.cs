﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace worktrack.Model
{
    public class ProjectsXmlRepository : IRepository<Project>
    {
        private List<Project> _projects;
        private const string FileName = "projects.xml";

        public ProjectsXmlRepository()
        {
            Deserialize();
        }

        #region Actions

        public IQueryable<Project> Query()
        {
            return _projects.AsQueryable();
        }

        public void Add(Project project)
        {
            _projects.Add(project);
        }

        public void Save()
        {
            Serialize();
        }

        public void DeleteById(Guid guid)
        {
            _projects.Remove(_projects.Single(p => p.Id == guid));
        }

        #endregion

        #region Serialization

        private void Serialize()
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(List<Project>));
            using (FileStream fileStream = File.Open(FileName, FileMode.Truncate, FileAccess.ReadWrite))
            {
                serializer.WriteObject(fileStream, _projects);
            }

        }

        private void Deserialize()
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(List<Project>));
            using (FileStream fileStream = File.Open(FileName, FileMode.OpenOrCreate, FileAccess.Read))
            {
                if (fileStream.Length > 0)
                {
                    _projects = (List<Project>)serializer.ReadObject(fileStream);
                }
                else
                {
                    _projects = new List<Project>();
                }
            }
        }

        #endregion
    }
}
