using System;
using System.Linq;

namespace worktrack.Model
{
    public interface IRepository<T>
    {
        IQueryable<T> Query();
        void Add(T project);
        void Save();
        void DeleteById(Guid guid);
    }
}