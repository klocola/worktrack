﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace worktrack.Model
{
    public class Project
    {
        public Guid Id { get; private set; }
        public string Name { get; set; }
        public TimeSpan TotalTime { get; set; }

        public Project()
        {
            Id = Guid.NewGuid();
        }
    }
}
