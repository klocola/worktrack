﻿using System.Windows;

namespace worktrack
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ProjectsListView : Window
    {
        public ProjectsListView()
        {
            InitializeComponent();
        }

        private void Worktrack_Loaded(object sender, RoutedEventArgs e)
        {
            Left = SystemParameters.WorkArea.Right - Width;
            Top = SystemParameters.WorkArea.Bottom - Height;
        }

        private void Worktrack_Deactivated(object sender, System.EventArgs e)
        {
        	WindowState = WindowState.Minimized;
        }
    }
}