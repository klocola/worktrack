﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using worktrack.Model;

namespace worktrack.ViewModel
{
    public class ProjectsListViewModel : ObservableCollection<ProjectViewModel>
    {
        private readonly IRepository<Project> _projectRepository;

        public ProjectsListViewModel()
        {
            StopTrackingCommand = new RelayCommand(StopTracking);
            _projectRepository = new ProjectsXmlRepository();
            foreach (var project in _projectRepository.Query())
            {
                Add(new ProjectViewModel() {Id = project.Id, Name = project.Name, TotalTime = project.TotalTime});
            }

            CollectionChanged += ProjectsListViewModel_CollectionChanged;
        }

        protected override void InsertItem(int index, ProjectViewModel item)
        {
            item.ProjectEndEdit += Item_ProjectEndEdit;
            base.InsertItem(index, item);
        }

        #region Event handlers

        private void Item_ProjectEndEdit(ProjectViewModel sender)
        {
            if (sender.Id == Guid.Empty)
            {
                AddNewProject(sender);
            }
            else
            {
                SaveProject(sender);
            }
        }

        private void ProjectsListViewModel_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                RemoveProjects(e.OldItems.Cast<ProjectViewModel>());
            }
        }

        #endregion

        #region Actions

        private void RemoveProjects(IEnumerable<ProjectViewModel> projects)
        {
            foreach (var project in projects)
            {
                if (project.Id != Guid.Empty)
                {
                    _projectRepository.DeleteById(project.Id);
                    _projectRepository.Save();
                }
                else
                {
                    Remove(project);
                }
            }
        }

        private void SaveProject(ProjectViewModel project)
        {
            Project editedProject = _projectRepository.Query().Single(p => p.Id == project.Id);
            editedProject.Name = project.Name;
            editedProject.TotalTime = project.TotalTime;
            _projectRepository.Save();
        }

        private void AddNewProject(ProjectViewModel project)
        {
            Project editedProject = new Project() {Name = project.Name, TotalTime = project.TotalTime};
            project.Id = editedProject.Id;
            _projectRepository.Add(editedProject);
            _projectRepository.Save();
        }
        
        #endregion

        #region Stop tracking command

        public ICommand StopTrackingCommand { get; set; }

        public void StopTracking()
        {
            foreach (var projectViewModel in Items.Where(p => p.IsTracking))
            {
                projectViewModel.StopTracking();
            }
        }

        #endregion
    }

}