﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;

namespace worktrack.ViewModel
{
    public class ProjectViewModel : ViewModelBase, IEditableObject, IDataErrorInfo
    {
        public ProjectViewModel()
        {
            ToggleTrackingCommand = new RelayCommand(ToggleTracking);
            _dispatcherTimer = new DispatcherTimer();
            _dispatcherTimer.Interval = TimeSpan.FromSeconds(1);
            _dispatcherTimer.Tick += new EventHandler(DispatcherTimer_Tick);
        }

        #region Project details

        public Guid Id { get; set; }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                Debug.Print(value);
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        private TimeSpan _totalTime;
        public TimeSpan TotalTime
        {
            get { return _totalTime; }
            set
            {
                _totalTime = value;
                OnPropertyChanged("TotalTime");
            }
        }

        #endregion

        #region IEditableObject

        public delegate void EndEditEventHandler(ProjectViewModel sender);

        public event EndEditEventHandler ProjectEndEdit;

        public void OnProjectEndEdit()
        {
            EndEditEventHandler handler = ProjectEndEdit;
            if (handler != null)
            {
                handler(this);
            }
        }

        public void BeginEdit()
        {
        }

        public void EndEdit()
        {
            OnProjectEndEdit();
        }

        public void CancelEdit()
        {
        }

        #endregion

        #region Tracking

        public ICommand ToggleTrackingCommand { get; set; }
        
        private bool _isTracking;
        private readonly DispatcherTimer _dispatcherTimer;
        private DateTime _startTime;
        private TimeSpan _oldTotalTime;

        private TimeSpan OperatingTime
        {
            get { return DateTime.UtcNow - _startTime; }
        }

        void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            TotalTime = _oldTotalTime + OperatingTime;
        }

        public bool IsTracking
        {
            get { return _isTracking; }
            set
            {
                _isTracking = value;
                OnPropertyChanged("IsTracking");
            }
        }

        public void ToggleTracking()
        {
            if (IsTracking)
            {
                StopTracking();
            }
            else
            {
                StartTracking();
            }
        }

        public void StartTracking()
        {
            _startTime = DateTime.UtcNow;
            _oldTotalTime = TotalTime;
            IsTracking = true;
            _dispatcherTimer.Start();
        }

        public void StopTracking()
        {
            _dispatcherTimer.Stop();
            TotalTime = _oldTotalTime + OperatingTime;
            IsTracking = false;
            OnProjectEndEdit();
        }

        #endregion

        #region IDataErrorInfo

        public string this[string columnName]
        {
            get
            {
                string result = null;
                if (columnName == "Name")
                {
                    if (string.IsNullOrEmpty(Name))
                    {
                        result = "You have to name your project.";
                    }
                }

                return result;
            }

        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}
