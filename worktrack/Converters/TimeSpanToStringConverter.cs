﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace worktrack.Converters
{
    public class TimeSpanToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            TimeSpan time = (TimeSpan) value;
            if (time.TotalDays >= 1)
            {
                return String.Format("{0}d {1}h ", time.Days, time.Hours);
            }
            else if (time.TotalHours >= 1)
            {
                return String.Format("{0}h {1}m ", time.Hours, time.Minutes);                
            }
            else if (time.TotalMinutes >= 1)
            {
                return String.Format("{0}m {1}s ", time.Minutes, time.Seconds);
            }
            else
            {
                return String.Format("{0}s ", time.Seconds);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

